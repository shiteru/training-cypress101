/// <reference types="cypress"/>

describe('User', () => {
    describe('Get User Collection', () => {
        it('It shoud return 200OK with user collection', () => {
            let host = Cypress.config().host;
            cy.request(host + '/api/users?page=2')
                .then((result) => {
                    expect(result.status).ok
                    expect(result.body.data).exist
                })
        })
    })
})