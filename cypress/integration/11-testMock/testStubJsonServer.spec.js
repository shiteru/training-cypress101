/// <reference types="cypress"/>

describe('User', () => {
    describe('Get User Collection', () => {
        it('It shoud return 200OK with user collection', () => {
            cy.request('http://localhost:3000/users')
                .then((result) => {
                    expect(result.status).ok
                    expect(result.body.data).exist
                })
        })
    })
})