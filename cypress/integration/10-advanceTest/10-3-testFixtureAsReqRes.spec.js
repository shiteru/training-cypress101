/// <reference types="cypress" />

describe('Fixture as Request', () => {
    it('It should reuturn 201 Created', () => {
        cy.fixture('10-3-reqBody.json')
            .then((bodyReq) => {
                cy.log("Body:" + JSON.stringify(bodyReq))
                cy.request({
                    method: 'POST',
                    url: 'https://reqres.in/api/users',
                    body: bodyReq
                }).then((result) => {
                    expect(result.status).to.be.eq(201)
                })
            })
    })
})

describe('Fixture as response', () => {
    it('It should return 200OK', () => {
        cy.fixture('10-3-fixtureResult.json')
            .then((baseResult) => {
                cy.request('https://reqres.in/api/users?page=2')
                    .then((result) => {
                        expect(result.status).to.be.eq(200)
                        expect(result.body).to.deep.eq(baseResult)
                    })
            })
    })
})