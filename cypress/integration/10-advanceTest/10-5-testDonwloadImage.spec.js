/// <reference types="cypress" />

describe('Download and Compare', () => {
    it('It should download file', () => {
        cy.request({
            method: 'GET',
            url: 'https://www.cypress.io/static/cypress-io-logo-social-share-8fb8a1db3cdc0b289fad927694ecb415.png',
            encoding: 'binary'
        }).then((result) => {
            expect(result.status).to.be.eq(200)
            cy.writeFile('cypress/fixtures/hello1.png', result.body,{
                encoding: 'binary',
                decodeContentFromBase64: true
            })

            cy.fixture('hello.png', 'binary')
            .then((baseResult) =>{
                cy.fixture('hello1.png', 'binary')
                .then((realResult) =>{
                    expect(baseResult).to.be.deep.eq(realResult)
                })
            })
        })
    })
})