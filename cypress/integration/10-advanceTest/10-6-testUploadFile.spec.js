/// <reference types="cypress" />

describe('Upload file', ()=>{
    it('It should be return 200OK when uoload file success', ()=>{
        cy.fixture('hello1.png', 'binary')
        .then(imageFile =>{
            const uploadedFile = Cypress.Blob.binaryStringToBlob(imageFile)

            const xhr = new XMLHttpRequest()
            xhr.open('POST', 'http://localhost:8080/employees', false)

            xhr.onload = function(){
                cy.log('reponse: ' + xhr.responseText)
                var reuslt = JSON.stringify(xhr.responseText)
                expect(xhr.status).to.be.eq(200)
            }

            xhr.onerror = function(){
                cy.log('reponse: ' + xhr.responseText)
                expect(xhr.status).to.be.eq(500)
            }

            const formData = new FormData();
            formData.set('multipartFile', uploadedFile)

            xhr.send(formData)

        }) 
        
    })
})