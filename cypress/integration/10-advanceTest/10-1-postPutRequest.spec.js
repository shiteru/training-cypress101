/// <reference types="cypress" />

describe('Post Request', ()=>{
    it('It shoule return 201 Created', ()=>{
        cy.request({
            method: 'POST',
            url: 'https://reqres.in/api/users',
            body:{
                "name": "morpheus",
                "job": "leader"
            }
        }).then((result)=>{
            expect(result.status).to.eq(201)
            expect(result.body.name).to.eq('morpheus')
            expect(result.body.job).to.eq('leader')
            expect(result.body.id).to.not.be.empty
        })
    })
})

describe('Put Request', ()=>{
    it('It shoule return 200 OK', ()=>{
        cy.request({
            method: 'PUT',
            url: 'https://reqres.in/api/users/2',
            body:{
                "name": "Johny",
                "job": "Engineer"
            }
        }).then((result)=>{
            expect(result.status).to.eq(200)
            expect(result.body.name).to.eq('Johny')
            expect(result.body.job).to.eq('Engineer')
            expect(result.body.updatedAt).to.not.be.empty
        })
    })
})

describe('Patch Request', ()=>{
    it('It shoule return 200 OK', ()=>{
        cy.request({
            method: 'PATCH',
            url: 'https://reqres.in/api/users/2',
            body:{
                "name": "Johny",
                "job": "Engineer"
            }
        }).then((result)=>{
            expect(result.status).to.eq(200)
            expect(result.body.name).to.eq('Johny')
            expect(result.body.job).to.eq('Engineer')
            expect(result.body.updatedAt).to.not.be.empty
        })
    })
})

describe('Delete Request', ()=>{
    it('It shoule return 204 No Content', ()=>{
        cy.request({
            method: 'DELETE',
            url: 'https://reqres.in/api/users/2'
        }).then((result)=>{
            expect(result.status).to.eq(204)
        })
    })
})