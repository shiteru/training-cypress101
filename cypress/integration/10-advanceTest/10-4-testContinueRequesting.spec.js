/// <reference types="cypress" />

describe('Test Continue Requesting', ()=>{
    it('It should return 200OK', ()=>{
        cy.request({
            method: 'POST',
            url: 'https://reqres.in/api/register',
            body:{
                "email": "eve.holt@reqres.in",
                "password": "pistol"
            }
        }).then((result) => {
            expect(result.status).to.be.eq(200)
            expect(result.body.token).to.be.not.empty

            cy.request({
                method: 'GET',
                url: 'https://reqres.in/api/users?page=2',
                headers:{
                    'authentication': 'Bearer ' + result.body.token
                }
            }).then((result2)=>{
                expect(result2.status).to.be.eq(200)
            })
        })
    })
})