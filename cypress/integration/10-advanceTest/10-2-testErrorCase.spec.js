/// <reference types="cypress" />

describe('Error Request', ()=>{
    it('It shoule return 404 Not Found', ()=>{
        cy.request({
            method: 'GET',
            url: 'https://reqres.in/api/unknown/23',
            failOnStatusCode: false        
        }).then((result)=>{
            expect(result.status).to.eq(404)
            expect(result.body).to.be.empty
        })
    })
})
