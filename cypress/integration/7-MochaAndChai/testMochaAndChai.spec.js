/// <reference types="cypress"/>

describe('User', () => {
    describe('Get User Collection', () => {
        it('It shoud return 200OK with user collection', () => {
            cy.request('https://reqres.in/api/users?page=2')
                .then((result) => {
                    expect(result.status).to.eq(200)
                    expect(result.body.data).to.have.lengthOf(6)
                })
        })
    })

    describe('Get User by Id', () => {
        it('It should return 200OK with user id equals 2', () => {
            cy.request('https://reqres.in/api/users/2')
                .then((result) => {
                    expect(result.status).to.eq(200)
                })
        })
    })
})