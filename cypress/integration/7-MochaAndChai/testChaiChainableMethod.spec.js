/// <reference types="cypress"/>

describe('User', () => {
    describe('Get User Collection', () => {
        it('It shoud return 200OK with user collection', () => {
            cy.request('https://reqres.in/api/users?page=2')
                .then((result) => {
                    expect(result.status).is.ok
                    expect(result.body.data).is.exist
                    expect(result.body.data).is.lengthOf(6)
                    expect(result.body.data).is.not.undefined

                    expect(result.body).to.not.have.any.keys('datas')

                    //Test Method
                    expect(result.body.data).length.above(5)
                    expect(result.body.data).length.below(7)

                    //Test Chainable Method
                    expect(result.body).to.include.keys('data')

                    expect({a:1,b:2}).to.include.keys('a')

                })
        })
    })
})