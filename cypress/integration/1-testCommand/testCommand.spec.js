/// <reference types="cypress"/>
before(() => {
    cy.request('https://reqres.in/api/users?page=2').as('req')
})

describe('Action Command: Request', ()=>{
    it('Test Request', ()=>{
        cy.get('@req')
        .then((result) => {
            expect(result.status).to.eq(200)   
            
            cy.get(result.body.data)
            .each((item) => {
                expect(item.email).to.contain('@')
            })
        })
        .should('not.be.empty')
        .should('not.be.null')
        
        cy.fixture('example.json')
        .then((result) => {
            cy.log('PrintResultFixture: ' + JSON.stringify(result))   
            
            cy.writeFile('abc.txt', result)
            cy.readFile('abc.txt')
            .then((content) => {
                cy.log("FileContent: " + content)
            })
        })

        cy.fixture('example.json')
        .then((result) =>{
            cy.log('At Fixture: ' + JSON.stringify(result)) 
            const dataConent = Cypress.Blob.binaryStringToBlob(result)
            cy.log('At Bolob:' + dataConent)
        })
    })
})