/// <reference types="cypress"/>
before(()=>{
    cy.log('At before')
})

beforeEach(()=>{
    cy.log('At before each')
    cy.request('https://reqres.in/api/users?page=2').as('getUserCollectionReq')
})

afterEach(()=>{
    cy.log('At After Each')
})

after(()=>{
    cy.log('At After')
})

describe('User', () => {
    describe('Get User Collection', () => {
        it.skip('It shoud return 200OK with user collection', () => {
            cy.get('@getUserCollectionReq')
                .then((result) => {
                    expect(result.status).to.eq(200)
                    expect(result.body.data).to.have.lengthOf(6)
                })
        })

        it('It shoud return 200OK with user collection', () => {
            cy.get('@getUserCollectionReq')
                .then((result) => {
                    expect(result.status).to.eq(200)
                    expect(result.body.data).to.have.lengthOf(6)
                })
        })
    })

    describe('Get User by Id', () => {
        it.skip('It should return 200OK with user id equals 2', () => {
            cy.request('https://reqres.in/api/users/2')
                .then((result) => {
                    expect(result.status).to.eq(200)
                })
        })
    })
})