/// <reference types="cypress"/>

describe('User', () => {
    describe('Get User Collection', () => {
        it('It shoud return 200OK with user collection', () => {
            cy.request('https://reqres.in/api/users?page=2')
                .then((result) => {
                    if (result.body.page == 3) {
                        cy.log("This is second page")
                        expect(result.status).ok
                        expect(result.body.data).exist
                    }else{
                        cy.log("This is other page")
                        expect(result.status).to.eq(500)
                    }
                })
        })
    })
})