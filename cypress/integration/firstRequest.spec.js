before(() => {
    cy.request('https://reqres.in/api/users?page=2').as('req')
})


describe('Simple First Request', () =>{   
    
    it('It should request to target website', () =>{
        cy.get('@req')
        .then((result) => {
            expect(result.status).to.eq(200)
            expect(result.body.page).to.eq(2)
            expect(result.body.total).to.eq(12)
            expect(result.body.total).to.eq(12)
        })    
    })

    it('It should request to target websites', () =>{
        cy.get('@req')
        .then((result) => {
            expect(result.status).to.eq(200)
            expect(result.body.page).to.eq(2)
            expect(result.body.total).to.eq(12)
            expect(result.body.total).to.eq(12)
        })    
    })
})
